package com.jaga888.scheduler;

import com.jaga888.dao.SettingDao;
import com.jaga888.dao.entity.Setting;
import com.jaga888.service.RssService;
import lombok.AllArgsConstructor;

import java.util.TimerTask;

@AllArgsConstructor
public class SchedulerMain extends TimerTask {

    private RssService rssService;

    private SettingDao settingDao;

    @Override
    public void run() {
        try {
            final var settings = settingDao.getAll();
            for (Setting setting : settings) {
                rssService.rss(setting);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}