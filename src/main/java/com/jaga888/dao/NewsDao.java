package com.jaga888.dao;

import com.jaga888.dao.entity.News;

import java.sql.SQLException;

public interface NewsDao {

    News save(News news) throws SQLException;

    void saveAll(News news) throws SQLException;
}
