package com.jaga888.dao;

import com.jaga888.dao.entity.News;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NewsDaoImpl implements NewsDao {

    private String sql = "INSERT INTO NEWS(TITLE,LINK,DESCRIPTION,PUB_DATE,CATEGORY) VALUES(?, ?, ?, ?, ?)";

    @Override
    public News save(News news) throws SQLException {
        try (final var connection = DataSource.getConnection();
             final var preparedStatement = connection.prepareStatement(sql)) {
            setNews(preparedStatement, news);
            preparedStatement.execute();

            return news;
        }
    }

    @Override
    public void saveAll(News news) throws SQLException {
        try (final var connection = DataSource.getConnection();
             final var preparedStatement = connection.prepareStatement(sql)) {
            connection.setAutoCommit(false);
            setNews(preparedStatement, news);
            preparedStatement.addBatch();
            preparedStatement.executeBatch();
            connection.commit();
        }
    }

    private void setNews(PreparedStatement preparedStatement, News news) throws SQLException {
        preparedStatement.setString(1, news.getTitle());
        preparedStatement.setString(2, news.getLink());
        preparedStatement.setString(3, news.getDescription());
        preparedStatement.setObject(4, news.getPubDate());
        preparedStatement.setString(5, news.getCategory());
    }
}
