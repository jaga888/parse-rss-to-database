package com.jaga888.dao;

import com.jaga888.dao.entity.Setting;

import java.sql.SQLException;
import java.util.List;

public interface SettingDao {

    Setting getById(int id) throws SQLException;

    List<Setting> getAll() throws SQLException;

    void update(Setting setting) throws SQLException;

}
