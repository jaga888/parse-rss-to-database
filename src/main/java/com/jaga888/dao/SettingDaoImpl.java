package com.jaga888.dao;

import com.jaga888.dao.entity.Setting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SettingDaoImpl implements SettingDao {

    @Override
    public Setting getById(int id) throws SQLException {
        final var sql = "SELECT * FROM SETTINGS WHERE ID = ?";
        final var setting = new Setting();

        try (final var connection = DataSource.getConnection();
             final var preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            final var resultSet = preparedStatement.executeQuery();
            settingSet(resultSet, setting);

            return setting;
        }
    }

    @Override
    public List<Setting> getAll() throws SQLException {
        final var settings = new ArrayList<Setting>();
        final var sql = "SELECT * FROM SETTING";

        try (final var connection = DataSource.getConnection();
             final var statement = connection.createStatement()) {
            final var resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Setting setting = new Setting();
                settingSet(resultSet, setting);
                settings.add(setting);
            }
        }

        return settings;
    }

    @Override
    public void update(Setting setting) throws SQLException {
        final var sql = "UPDATE SETTING SET LINK=?, TIME=?, LAST_TIME=?, LAST_UPDATE=? WHERE ID=?";
        try (final var connection = DataSource.getConnection();
             final var preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, setting.getLink());
            preparedStatement.setInt(2, setting.getTimeOfUpdate());
            preparedStatement.setInt(3, setting.getTimeOfPast());
            preparedStatement.setString(4, setting.getLastUpdate());
            preparedStatement.setLong(5, setting.getId());
            preparedStatement.executeUpdate();
        }
    }


    private void settingSet(ResultSet resultSet, Setting setting) throws SQLException {
        setting.setId(resultSet.getLong("ID"));
        setting.setLink(resultSet.getString("LINK"));
        setting.setTimeOfUpdate(resultSet.getInt("TIME"));
        setting.setTimeOfPast(resultSet.getInt("LAST_TIME"));
        setting.setLastUpdate(resultSet.getString("LAST_UPDATE"));
    }
}
