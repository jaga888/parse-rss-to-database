package com.jaga888.dao.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class News {

    private String title;
    private String link;
    private String description;
    private LocalDateTime pubDate;
    private String category;
}
