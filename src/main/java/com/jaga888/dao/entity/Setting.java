package com.jaga888.dao.entity;

import lombok.Data;

@Data
public class Setting {

    private long id;
    private String link;
    private int timeOfUpdate;
    private int timeOfPast;
    private String lastUpdate;
}
