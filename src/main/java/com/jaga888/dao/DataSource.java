package com.jaga888.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {

    private static final String URL = "jdbc:h2:~/test";

    private static final String USER = "sa";

    private static final String PASS = "";

    public static final String JDBC_DRIVER = "org.h2.Driver";

    public static Connection getConnection() {
        Connection connection;
        try {
            connection = DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;
    }
}
