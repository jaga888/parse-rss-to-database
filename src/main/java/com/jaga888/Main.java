package com.jaga888;

import com.jaga888.component.RssParser;
import com.jaga888.dao.DataSource;
import com.jaga888.dao.NewsDaoImpl;
import com.jaga888.dao.SettingDaoImpl;
import com.jaga888.scheduler.SchedulerMain;
import com.jaga888.service.RssService;

import javax.xml.parsers.DocumentBuilderFactory;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Timer;

public class Main {

    public static void main(String[] args) throws Exception {
        Class.forName(DataSource.JDBC_DRIVER);

        final var content = new String(Files.readAllBytes(Paths.get(Main.class.getClassLoader().getResource("script.sql").toURI())));
        final var statement = DataSource.getConnection().createStatement();
        statement.execute(content);

        final var settingDao = new SettingDaoImpl();
        final var newsDao = new NewsDaoImpl();
        final var rssParser = new RssParser(DocumentBuilderFactory.newInstance());
        final var rssService = new RssService(settingDao, rssParser, newsDao);
        final var timer = new Timer();

        final var sm = new SchedulerMain(rssService, settingDao);
        timer.schedule(sm, 1000 * 60);
        sm.run();
    }
}