package com.jaga888.component;

import com.jaga888.dao.entity.News;
import lombok.AllArgsConstructor;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class RssParser {

    private DocumentBuilderFactory documentBuilderFactory;

    public List<News> parse(String url) throws ParserConfigurationException, IOException, SAXException {
        final var documentBuilder = documentBuilderFactory.newDocumentBuilder();
        final var document = documentBuilder.parse(url);
        final var root = document.getDocumentElement();
        final var nodeNews = root.getElementsByTagName("item");
        return createNews(nodeNews);
    }

    private List<News> createNews(NodeList nodeNews) {
        final var size = nodeNews.getLength();
        final var newsList = new ArrayList<News>(size);
        for (int i = 0; i < nodeNews.getLength(); i++) {
            final var nodeItem = nodeNews.item(i);
            final var news = new News();
            if (nodeItem.getNodeType() != Node.TEXT_NODE) {
                final var nodeList = nodeItem.getChildNodes();
                for (int j = 0; j < nodeList.getLength(); j++) {
                    final var list = nodeList.item(j);
                    if (list.getNodeType() != Node.TEXT_NODE) {

                        addToNews(list, news);
                    }
                }
                newsList.add(news);
            }
        }
        return newsList;
    }

    private void addToNews(Node list, News news) {
        switch (list.getNodeName()) {
            case "title":
                news.setTitle(list.getTextContent());
                break;

            case "link":
                news.setLink(list.getTextContent());
                break;

            case "description":
                news.setDescription(list.getTextContent());
                break;

            case "pubDate":
                ZonedDateTime zdt = ZonedDateTime.parse(list.getTextContent(), DateTimeFormatter.RFC_1123_DATE_TIME);
                LocalDateTime time1 = zdt.toLocalDateTime();
                news.setPubDate(time1);
                break;

            case "category":
                news.setCategory(list.getTextContent());
                break;
        }
    }
}