package com.jaga888.service;

import com.jaga888.component.RssParser;
import com.jaga888.dao.NewsDao;
import com.jaga888.dao.SettingDao;
import com.jaga888.dao.entity.News;
import com.jaga888.dao.entity.Setting;
import lombok.AllArgsConstructor;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@AllArgsConstructor
public class RssService {

    private SettingDao settingDao;

    private RssParser rssParser;

    private NewsDao newsDao;

    public void rss(Setting setting) throws Exception {
        try {
            var time = 0;
            final var lastTime = setting.getTimeOfPast();

            if (lastTime == 0) {
                String url = setting.getLink();
                final var newsList = rssParser.parse(url);

                for (News news : newsList) {
                    if (setting.getLastUpdate().compareTo(news.getPubDate().toString()) < 0) {
                        newsDao.saveAll(news);
                    }
                }
                final var ldt = LocalDateTime.now();
                final var ld = ldt.toLocalDate();
                final var dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
                setting.setLastUpdate(ld + "T" + LocalTime.now().format(dtf));

                time = setting.getTimeOfUpdate() - 1;
                setting.setTimeOfPast(time);
                settingDao.update(setting);
            } else {
                setting.setTimeOfPast(lastTime - 1);
                settingDao.update(setting);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
