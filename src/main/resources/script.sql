CREATE TABLE IF NOT EXISTS NEWS
(
  ID          long NOT NULL auto_increment,
  TITLE       VARCHAR(255),
  LINK        VARCHAR(255),
  DESCRIPTION VARCHAR2(4096),
  PUB_DATE    TIMESTAMP,
  CATEGORY    VARCHAR(255),
  PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS SETTING
(
  ID          long NOT NULL auto_increment,
  LINK        VARCHAR(45),
  TIME        int,
  LAST_TIME   int,
  LAST_UPDATE VARCHAR(45),
  PRIMARY KEY (ID)
);

INSERT INTO SETTING(LINK, TIME, LAST_TIME, LAST_UPDATE)
VALUES ('https://plainnews.ru/rss/all', 2, 0, '2000-01-01T21:55:36'),
       ('https://lenta.ru/rss', 1, 0, '2000-01-01T21:55:36');

